import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListPilotsPageRoutingModule } from './list-pilots-routing.module';

import { ListPilotsPage } from './list-pilots.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListPilotsPageRoutingModule
  ],
  declarations: [ListPilotsPage]
})
export class ListPilotsPageModule {}
