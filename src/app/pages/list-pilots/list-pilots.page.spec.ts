import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListPilotsPage } from './list-pilots.page';

describe('ListPilotsPage', () => {
  let component: ListPilotsPage;
  let fixture: ComponentFixture<ListPilotsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPilotsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListPilotsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
