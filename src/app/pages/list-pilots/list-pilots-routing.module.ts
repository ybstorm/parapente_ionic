import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListPilotsPage } from './list-pilots.page';

const routes: Routes = [
  {
    path: '',
    component: ListPilotsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListPilotsPageRoutingModule {}
