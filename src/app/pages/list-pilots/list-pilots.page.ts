import { Component, OnInit } from '@angular/core';
import { PilotServiceService } from 'src/app/Services/pilot-service.service';
import { NavController } from '@ionic/angular';
import {Pilot} from 'src/app/models/pilot';
@Component({
  selector: 'app-list-pilots',
  templateUrl: './list-pilots.page.html',
  styleUrls: ['./list-pilots.page.scss'],
})
export class ListPilotsPage implements OnInit {
pilots : Pilot [];
  constructor(private pilotService :PilotServiceService,private navController: NavController) { }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.pilotService.getPilots().subscribe(data => this.pilots = data);
  }
  get rows(): Array<Pilot[]> {
    const aRows = [];

    const nbRows = (this.pilots.length % 4);

    for (let i = 0; i < nbRows; i++) {
      const row = [];
      for (let j = 0; j < 4; j++) {
        if (this.pilots[i + j]) {
          row.push(this.pilots[i + j]);
        }
      }
      aRows.push(row);
    }
    return aRows;
  }
}
