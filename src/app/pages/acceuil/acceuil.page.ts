import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.page.html',
  styleUrls: ['./acceuil.page.scss'],
})
export class AcceuilPage implements OnInit {

  constructor(public navCtrl: NavController) { }

  ngOnInit() {
  }
  onGoToPilots(){
    this.navCtrl.navigateRoot('list-pilots');
  }

}
