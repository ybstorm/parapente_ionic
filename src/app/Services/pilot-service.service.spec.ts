import { TestBed } from '@angular/core/testing';

import { PilotServiceService } from './pilot-service.service';

describe('PilotServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PilotServiceService = TestBed.get(PilotServiceService);
    expect(service).toBeTruthy();
  });
});
