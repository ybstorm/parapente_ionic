import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Pilot} from 'src/app/models/pilot';
@Injectable({
  providedIn: 'root'
})
export class PilotServiceService {

  constructor(private httpClient: HttpClient) { }
  getPilots(): Observable<Pilot[]> {
    return this.httpClient.get<Pilot[]>(`/api/pilotes`).pipe(
        tap(_ => console.log('Pilot Get All')),
        catchError(this.handleError<Pilot[]>('Get All Pilot', []))
    );
  }
  
  private handleError<T>(operation= 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
}
}
