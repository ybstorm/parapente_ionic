export class Pilot {
    id: number;
    name: {
      first: string,
      last: string
    };
    address: string;
    phone: string;
    weight: number;
}
